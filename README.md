## Development server

Cliente:

primero instalar las dependecias con `npm install`

luego:

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Server:

primero instalar las dependecias con `npm install`

luego:

Run `npm run dev` for a dev server. run in `http://localhost:4000/`. The server will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Explicacion del desarrollo:

El FrontEnd se realizo en Angular y Angular Material.

El BackEnd esta hecho en Node JS con el framework Express. No se implemento una Base de datos por cuestiones de simplicidad y tiempo.

## Funcionalidad de la app:

Desde el front se seleccionan los adicionales para el cafe, luego se llama a la api hecha en node, al metodo "cobrar", al cual se le pasa por body en el metodo POST de HTTP, los adicionales elegidos.

En el backend se hace el calculo del valor total del cafe, devolviendo el resultado al front.

El front muestra el precio total en un alert de javascript.
