import { Component } from '@angular/core';
import { CobrarService } from './cobrar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  leche = false;
  cacao = false;
  chocolate = false;
  ron = false;
  constructor(private cobrarService:CobrarService) {

  }

  comprar() {
    this.cobrarService.cobrar(this.leche, this.cacao, this.chocolate, this.ron).subscribe((total) => {
      alert(`El total a pagar de su cafe es: $${total}`)
    })
  }

}
