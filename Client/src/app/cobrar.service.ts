import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
const api: string = environment.api;

@Injectable({
  providedIn: 'root'
})
export class CobrarService {

  constructor(private http:HttpClient) { }

  cobrar (leche, cacao, chocolate, ron) {
    return this.http.post(`${api}/cobrar`, {leche:leche, cacao:cacao, chocolate:chocolate, ron:ron});
  }
}
