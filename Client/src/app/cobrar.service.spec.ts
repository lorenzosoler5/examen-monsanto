import { TestBed } from '@angular/core/testing';

import { CobrarService } from './cobrar.service';

describe('CobrarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CobrarService = TestBed.get(CobrarService);
    expect(service).toBeTruthy();
  });
});
