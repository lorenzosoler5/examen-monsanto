import { Request, Response } from 'express';
import request from 'request';

const LECHE = 5
const CACAO = 10
const CHOCOLATE = 15
const RON = 20
export class ItemsController {
    cobrar (data:any) {
        return new Promise((resolve, reject) => {
            let total = 0;
            if (data.leche) {
                total = total + LECHE
            }
            if (data.cacao) {
                total = total + CACAO
            }
            if (data.chocolate) {
                total = total + CHOCOLATE
            }
            if (data.ron) {
                total = total + RON
            }
            resolve(total)
        })
    }
}