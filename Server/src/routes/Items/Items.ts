import express, { Request, Response } from 'express';
import { itemsController } from '../../controllers';

export const router = express.Router({
    strict: true
});

// Recibe un string como parametro 'q' para realizar la busqueda de productos
router.post('/', (req: Request, res: Response) => {
  itemsController.cobrar(req.body).then((resp) => {
      res.json(resp)
  }).catch(err => res.sendStatus(500).json(err))
});
