import express from 'express';
import { PORT } from './config/constants';
import { itemsRouter } from './routes';
import cors from 'cors'

const app = express();
app.use(cors());
app.use(express.json());

// Endpoint para los metodos
app.use('/cobrar', itemsRouter);


app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});